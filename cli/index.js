#!/usr/bin/env node

const program = require("commander");
const Db = require("./lib/db");
const Npm = require("./lib/npm");
const CommandError = require("./lib/commandError");


[Db, Npm].forEach(klass => {
    Object.keys(klass.COMMANDS).forEach(i => {
        let command = program.command(i)
            .option("-s, --service <name>", "Target specific service")
            .action(klass.invoke);

        if (klass.COMMANDS[i] && klass.COMMANDS[i].arguments) {
            command = command.arguments(klass.COMMANDS[i].arguments);
        }
    });
});


program.on("command:*", () => {
    CommandError(program.args.join(" "));
});

program
    .name("Services CLI")
    .parse(process.argv);
