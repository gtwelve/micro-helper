const Services = require("./services");
const CommandError = require("./commandError");
const COMMANDS = {
    "npm:install": null,
    "npm:clean": null,
    "npm:link": { arguments :"<name>" },
    "npm:unlink": { arguments :"<name>" }
};


module.exports = {
    COMMANDS,
    invoke: async (...args) => {
        let cmdObj = args[args.length - 1];
        let command = cmdObj.name().replace(":", " ");

        switch(cmdObj.name()) {
            case "npm:link":
            case "npm:unlink":
                command += (" " + args[0]);
                break;
            case "npm:clean":
                command = "rm -rf ./node_modules ./package-lock.json";
                break;
        }

        await Services.runCommand(cmdObj.service, command);
   }
};
