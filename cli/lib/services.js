const fs = require("fs");
const path = require("path");
const exec = require("child_process").exec;


class Services {


    getServiceList(service, requiredDir) {
        return this._getDirs(Services.getServiceDir()).then(list => {
            if (service) {
                return list.includes(service) ? [service] : [];
            }

            return list;
        }).then(services => {
            if (requiredDir) {
                let dirPrefix = "";

                if (requiredDir.includes("/")) {
                    dirPrefix = "/" + requiredDir.substring(0, requiredDir.lastIndexOf("/"));
                    requiredDir = requiredDir.substring(requiredDir.lastIndexOf("/") + 1);
                }

                return Promise.all(services.map(i => {
                    let loopDir = Services.getServiceDir(i) + dirPrefix;

                    return this._getDirs(loopDir).then(o => {
                        return o.includes(requiredDir) ? i : null;
                    }).catch(err => {
                        return Promise.resolve(null);
                    });
                })).then(filteredServices => {
                    return filteredServices.filter(i => i);
                });
            }

            return services;
        });
    }


    async runCommandForServices(services, command) {
        for (let i = 0; i < services.length; i++) {
            let fullCommand = `cd ${Services.getServiceDir(services[i])} && ${command}`;

            await this._runServiceCommand(services[i], fullCommand);
        }
    }


    _runServiceCommand(service, fullCommand) {
        console.info(`Run command for %s: %s`, service, fullCommand);

        return new Promise((resolve, reject) => {
            const child = exec(fullCommand, (err, stdout, stderr) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });

            child.stdout.pipe(process.stdout);
            child.stderr.pipe(process.stderr);
        });
    }


    _getDirs(path) {
        return new Promise((resolve, reject) => {
            fs.readdir(path, { withFileTypes: true }, (err, results) => {
                if (err) {
                    return reject(err);
                }

                resolve(results.filter(i => i.isDirectory()).map(i => i.name));
            });
        });
    }


    static getServiceDir(service) {
        return path.normalize(process.cwd() + "/services") + (service ? `/${service}` : "");
    }


    static async runCommand(service, command, requiredDir) {
        let services = new Services();
        let serviceList = await services.getServiceList(service, requiredDir);

        await services.runCommandForServices(serviceList, command);
    }


}


module.exports = Services;
