module.exports = (err) => {
    if (typeof(err) == "string") {
        console.error("Invalid command: %s\n\nSee --help for a list of available commands.\n", err);
    } else {
        console.error("Command error: \n\n%s\n", err.message);
    }
    
    process.exit(1);
};
