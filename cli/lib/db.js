const os = require("os");
const fs = require("fs");
const Services = require("./services");
const CommandError = require("./commandError");
const Config = require("../../lib/helpers/config");
const DbBuilder = require("../../lib/db/builder");
const COMMANDS = {
    "db:migrate": null,
    "db:migrate:undo": null,
    "db:migrate:undo:all": null,
    "db:seed:all": null,
    "db:seed:undo": null,
    "db:seed:undo:all": null
};


module.exports = {
    COMMANDS,
    invoke: async (cmdObj) => {
        const name = cmdObj.name();
        const folder = "src/db/" + (name.includes(":seed:") ? "seeders" : "migrations");
        const services = new Services();
        const serviceList = await services.getServiceList(cmdObj.service, folder);

        for (let i = 0; i < serviceList.length; i++) {
            let configPath = os.tmpdir() + `/db-config-${serviceList[i]}.json`;

            fs.writeFileSync(configPath, JSON.stringify({
                "migration": {
                    ...DbBuilder.DEFAULTS,
                    ...Config.get("db"),
                    migrationStorageTableName: DbBuilder.migrationsTableName(serviceList[i])
                }
            }));

            let migrationsPath = `${Services.getServiceDir(serviceList[i])}/src/db/migrations`;
            let seedersPath = `${Services.getServiceDir(serviceList[i])}/src/db/seeders`;
            let commandParts = [
                `./node_modules/.bin/sequelize-cli ${name}`,
                `--env migration`,
                `--config ${configPath}`,
                `--migrations-path ${migrationsPath}`,
                `--seeders-path ${seedersPath}`
            ];

            await services.runCommandForServices([serviceList[i]], commandParts.join(" "));

            try { fs.unlinkSync(configPath); } catch(e) {}
        }
   }
};
