const moment = require("moment-timezone");

const TIME_FORMAT = "HH:mm:ss";
const DATE_FORMAT = "YYYY-MM-DD";
const SERIALIZE_DATE_FORMAT = "YYYY-MM-DDTHH:mm:ss.SSSZ";
const SQL_FORMAT = "YYYY-MM-DD HH:mm:ss";


moment.prototype.timeString = function() {
    return this.utc().format(TIME_FORMAT);
};


moment.prototype.dateString = function() {
    return this.utc().format(DATE_FORMAT);
};


moment.prototype.serializeString = function() {
    return this.utc().format(SERIALIZE_DATE_FORMAT);
};


moment.prototype.sqlString = function() {
    return this.utc().format(SQL_FORMAT);
};


moment.parse = function(input) {
    let date = moment(input);

    return date.isValid() ? date.utc() : null;
};


module.exports = moment;
