const { ServiceException } = require("./exceptions");

let brokerInstance = null;


module.exports = {
    register(broker) {
        brokerInstance = broker;
    },
    get() {
        return brokerInstance;
    },
    call(...args) {
        let broker = this.get();
        
        if (!broker) {
            throw new ServiceException();
        }
        
        return broker.call.apply(broker, args);
    }
};
