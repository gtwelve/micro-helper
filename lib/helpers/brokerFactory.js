const Config = require("./config");


const transporterConfig = () => {
    if (Config.get("nats")) {
        let host = Config.get("nats.host") || "127.0.0.1";
        let port = Config.get("nats.port") || "4222";
        let user = Config.get("nats.user");
        let pass = Config.get("nats.password");

        return {
            type: "NATS",
            options: {
                url: `nats://${host}:${port}`,
                user: user,
                pass: pass
            }
        };
    }

    return "nats://0.0.0.0:4222";
};


module.exports = (moleculer, service, config) => {
    const broker = new moleculer.ServiceBroker({
        nodeID: service.name,
        cacher: "memory",
        logger: console,
        transporter: transporterConfig(),
        ...config
    });

    broker.createService(service);

    return broker;
};
