const fs = require("fs");
const { URLSearchParams } = require("url");
const fetch = require("node-fetch");
const AbortController = require("abort-controller");
const { ApplicationException } = require("./exceptions");


const TEXT_TYPE = "TEXT";
const JSON_TYPE = "JSON";
const FORM_TYPE = "FORM";


class Request {


    constructor(url, headers, data) {
        this.url = url;
        this.headers = headers || {};
        this.method = "GET";
        this.data = data || null;
        this.dataType = JSON_TYPE;
        this.timeout = 10000;
        this.maxResponseSize = 0;
        this.abortController = null;

        this.requested = false;
        this.responded = false;
        this.cancelled = false;
    }


    setMethod(value) {
        return this._setProp("method", value.toUpperCase());
    }


    setUrl(value) {
        return this._setProp("url", value);
    }


    setHeader(key, value) {
        this.headers[key] = value;

        return this;
    }


    setHeaders(value) {
        return this._setProp("headers", value);
    }


    setData(value, type) {
        this._setProp("data", value);

        if (type) {
            this.setDataType(type);
        }

        return this;
    }


    setDataType(type) {
        this.dataType = type;

        return this;
    }


    setTimeout(value) {
        return this._setProp("timeout", value);
    }


    setMaxResponseSize(value) {
        return this._setProp("maxResponseSize", value);
    }


    head() {
        return this._requestTextResponse("HEAD", null);
    }


    get() {
        return this._requestTextResponse("GET", null);
    }


    post(data) {
        return this._requestTextResponse("POST", data);
    }


    put(data) {
        return this._requestTextResponse("PUT", data);
    }


    dispatch(method, data) {
        return this._requestTextResponse(method, data);
    }


    delete() {
        return this._requestTextResponse("DELETE", null);
    }


    // Does not work with size option
    stream(stream) {
        stream.once("close", () => {
            this._onResponded();
        });

        this._startRequest().then(response => {
            response.body.pipe(stream);
        }).catch(err => {
            stream.emit("error", err);
            stream.destroy();

            if (stream.path) {
                fs.unlinkSync(stream.path);
            }
        });

        return stream;
    }


    cancel() {
        if (this.abortController) {
            this.abortController.abort();
        }

        this.cancelled = true;
        this._onResponded();
    }



    _requestTextResponse(method, data) {
        return this._startRequest(method, data).then(response => {
            return this._resolveBody(response).then(body => {
                this._onResponded();

                return Promise.resolve(new RequestResponse(response, body));
            }).catch(err => {
                this._onResponded();

                return this._reject(err);
            });
        });
    }



    _startRequest(method, data) {

        if (this.requested) {
            return this._reject(new RequestError("Request has already been made"));
        }

        this.requested = true;
        this.abortController = new AbortController();

        let params = this._getParams(method, data);
        let req = fetch(this.url, params).then((response) => {
            if (response.status < 200 || response.status >= 400) {
                return this._resolveBody(response).then(body => {
                    return Promise.reject(new RequestError(`Request resulted in ${response.status} code.`, body));
                });

            } else {
                return Promise.resolve(response);

            }
        }).catch(err => {
            return this._reject(err);
        });

        return req;


        // });


        //     let totalBytesRead = 0,
        //         timeoutRef = null,
        //         resolved = false,
        //         req = null;
        //
        //     let resolveWrapper = (response) => {
        //         if (!resolved) {
        //             req = null;
        //             resolved = true;
        //             resolve(response);
        //         }
        //     };
        //
        //     let rejectWrapper = (err) => {
        //         if (!resolved) {
        //             if (req) {
        //                 req.abort();
        //             }
        //             req = null;
        //             resolved = true;
        //             reject(err);
        //         }
        //     };
        //
        //     let requestConfig = {
        //         method: this.method,
        //         url: this.url,
        //         headers: this.headers,
        //         timeout: this.timeout
        //     };
        //
        //     if (this.responseType == JSON_TYPE) {
        //         requestConfig.json = true;
        //     }
        //
        //     if (this.data && this.postType == JSON_TYPE) {
        //         requestConfig.headers["Content-Type"] = "application/json";
        //         requestConfig.json = this.data;
        //     } else if (this.data && this.postType == FORM_TYPE) {
        //         requestConfig.headers["Content-Type"] = "multipart/form-data";
        //         requestConfig.formData = this.data;
        //     } else if (this.data) {
        //         requestConfig.body = this.data;
        //     }
        //
        //     console.log(`Making ${requestConfig.method} request to ${requestConfig.url}`);
        //
        //     req = request(requestConfig, (err, response, body) => {
        //         if (err) {
        //             err.response = response;
        //
        //             return rejectWrapper(err);
        //         } else if (response.statusCode < 200 || response.statusCode >= 400) {
        //
        //             console.log("response.body", response.body);
        //
        //             // TODO: name micro exception
        //             let error = new Error(response.body || `Request was not successful (${response.statusCode})`);
        //                 error.response = response;
        //
        //             return rejectWrapper(error);
        //         }
        //
        //         resolveWrapper(response);
        //     });
        //
        //     if (this.maxResponseSize) {
        //         // Check the response is not longer than max length, if it is it
        //         // might not be a streaming file so bail
        //         req.on("data", (data) => {
        //             totalBytesRead += data.length;
        //
        //             if (totalBytesRead >= this.maxResponseSize) {
        //                 // TODO: name micro exception
        //                 let error = new Error("Request exceeds max size");
        //                     error.response = response;
        //
        //                 rejectWrapper(error);
        //             }
        //         });
        //     }
        // });
    }


    _onResponded() {
        this.responded = true;
        this.abortController = null;
    }


    _setProp(prop, value) {
        this[prop] = value;

        return this;
    }


    _getParams(method, data, controller) {
        if (method) { this.method = method; }
        if (data) { this.data = data; }

        let params = {
            method: this.method,
            headers: this.headers || {},
            timeout: this.timeout,
            size: this.maxResponseSize,
            signal: this.abortController ? this.abortController.signal : null
        };

        if (this.data && this.dataType == JSON_TYPE) {
            params.headers["Content-Type"] = "application/json";
            params.body = typeof(this.data) != "string" ? JSON.stringify(this.data) : this.data;

        } else if (this.data && this.dataType == FORM_TYPE) {
            params.headers["Content-Type"] = "multipart/form-data";
            params.body = new URLSearchParams();

            for (let key in this.data) {
                params.body.append(key, this.data[key]);
            }
        } else if (this.data) {
            params.body = this.data;
        }

        // console.log("params", params);

        return params;
    }


    _resolveBody(response) {
        let contentType = response.headers.get("content-type");
        let bodyResolver = null;

        if (contentType.startsWith("application/json")) {
            bodyResolver = response.json();
        } else {
            bodyResolver = response.text();
        }

        return bodyResolver;
    }


    _reject(err) {
        if (err instanceof RequestError) {
            return Promise.reject(err);
        }

        return Promise.reject(new RequestError(err.message, err.data));
    }


    static engine() {
        return got;
    }


}



class RequestResponse {


    constructor(response, body) {
        this.code = response.status;
        this.status = response.status;
        this.statusText = response.statusText;
        this.headers = response.headers.raw();
        this.body = body;
        this.rawResponse = response;
    }


}


class RequestError extends ApplicationException {

    constructor(message, data) {
        super(message, 400, null, data);
    }

}




module.exports = Request;
