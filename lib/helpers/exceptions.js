const ExtendableError = require("es6-error");


class ApplicationException extends ExtendableError {

    constructor(message, code, type, data) {
        super(message);
        this.code = code || 500;
        this.type = type || "APPLICATION_EXCEPTION";
        this.data = data;
    }

}


class MaskedException extends ApplicationException {

    constructor(err) {
        super((err.message || "An unexpected error occured"), 400, (err.type || "APPLICATION_EXCEPTION"), null);
        
        this.name = "ApplicationException";

        if (process.env.NODE_ENV !== "production") {
            console.log("Masked Exception Error", err);
        }
    }

}


class ResourceMissingException extends ApplicationException {

    constructor(data) {
        super("Resource not found", 404, "RESOURCE_NOT_FOUND", data);
    }

};


class PersistanceException extends ApplicationException {

    constructor(message, data) {
        super(message, 500, "PERSISTANCE_EXCEPTION", data);
    }

}


class RemoteApiException extends ApplicationException {

    constructor(data) {
        super("A remote api encountered an error", 500, "REMOTE_API_EXCEPTION", data);
    }

}


class UnAuthorizedException extends ApplicationException {

    constructor(type, data) {
        super("Unauthorized Access", 401, (type || "UNAUTHORIZED"), data);
    }

};


class ForbiddenException extends ApplicationException {

    constructor(data, type) {
        super("Access Forbidden", 403, (type || "FORBIDDEN"), data);
    }

};


class OauthException extends ApplicationException {

    constructor(code, type, data) {
        super("Unauthorized Access", (code || 401), (type || "UNAUTHORIZED"), data);
    }

};



class ServiceException extends ApplicationException {

    constructor(data) {
        super("Service Unavailable", 500, "SERVICE_UNAVAILABLE", data);
    }

}


module.exports = {
    ApplicationException,
    MaskedException,
    ResourceMissingException,
    PersistanceException,
    RemoteApiException,
    UnAuthorizedException,
    ForbiddenException,
    OauthException,
    ServiceException
};
