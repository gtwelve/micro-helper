const fs = require("fs");
const dotEnv = require("dotenv");
const camelCase = require("camelcase");

const GROUP_SEPERATOR = ".";


class Config {


    constructor() {
        this._config = {};
        this._envFile = process.cwd() + "/.env";

        this.set("env", (process.env.NODE_ENV || "production"));
        this.set("timezone", (process.env.TZ || "Etc/UTC"));
        this.loadEnv();
    }


    loadEnv() {
        try {
            const envContents = fs.readFileSync(this._envFile);
            const envParsed = dotEnv.parse(envContents);

            for (let key in envParsed) {
                this.set(key.replace("_", "."), envParsed[key]);
            }
        } catch(err) {
            console.warn("No .env file found");
        }

        return this;
    }


    get(key, defaultValue) {
        let { configKey, group } = this._getKeyPath(key);

        if (defaultValue == undefined) {
            defaultValue = null;
        }

        if (group) {
            return (this._config[group] ? this._config[group][configKey] : defaultValue) || defaultValue;
        } else {
            return this._config[configKey] || defaultValue;
        }
    }


    set(key, value) {
        let { configKey, group } = this._getKeyPath(key);

        if ("timezone" == key) {
            process.env.TZ = value;
        }

        if (group) {
            if (!this._config[group]) {
                this._config[group] = {};
            }

            this._config[group][configKey] = value;
        } else {
            this._config[configKey] = value;
        }

        return this;
    }


    fill(values) {
        for (let key in values) {
            this.set(key, values[key]);
        }
    }


    _getKeyPath(key) {
        let configKey = key,
            group = null;

        if (key.includes(GROUP_SEPERATOR)) {
            group = key.substring(0, key.indexOf(GROUP_SEPERATOR)).toLowerCase();
            configKey = camelCase(key.substring(key.indexOf(GROUP_SEPERATOR) + 1));
        } else {
            configKey = camelCase(key);
        }

        return { configKey, group };
    }


}


module.exports = new Config();
