const { ApplicationException, MaskedException } = require("../helpers/exceptions");


module.exports = {


    /**
	 * Methods
	 */
	methods: {

        statusCode(ctx, code) {
            ctx.meta.$statusCode = code;
        },

        emptyResponse(ctx) {
            this.statusCode(ctx, 204);

            return null;
        },

		rejectMasked(error, maskedError) {
			if (maskedError && error.constructor.name === maskedError.constructor.name) {
				return Promise.reject(error);
			} else if (maskedError) {
				return Promise.reject(maskedError);
			}

			return Promise.reject(error);
		},

		reject(err) {
			if ((err instanceof ApplicationException) === false) {
				return this.rejectMasked(err, new MaskedException(err));
			}

			return this.rejectMasked(err);
		}


    }


};
