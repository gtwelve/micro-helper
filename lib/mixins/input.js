"use strict";


module.exports = {


	/**
	 * Methods
	 */
	methods: {


		getString(param) {
			return typeof(param) == "string" && param != "" ? param : null;
		},


		getInt(param) {
			return isNaN(param) ? null : parseInt(param);
		},


		getPage(ctx) {
			let page = this.getInt(ctx.params && ctx.params.page);

			return page && page > 1 ? page : 1;
		},


		getPerPage(ctx) {
			let perValue = ctx.params && ctx.params.per_page;
			if (perValue === -1) {
				return -1;
			}

			let per = this.getInt(perValue);
			if (per && per >= 1 && per < 100) {
				return per;
			}

			return 20;
		},


		getBodyParam(ctx, path) {
			try {
				return path.split(".").reduce((acc, i) => {
					return acc ? (acc[i] ? acc[i] : null) : null;
				}, ctx.params);
			} catch(err) {
				return null;
			}
		}


	}


};
