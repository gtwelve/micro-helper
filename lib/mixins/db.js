"use strict";

const DbBuilder = require("../db/builder");


module.exports = (...models) => {
	return {
		
		created() {
			this.db = DbBuilder.build(this.name, this.broker, null, ...models);
		}

	};
};
