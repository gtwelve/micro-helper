const Sequelize = require("sequelize");
const camelCase = require("camelcase");
const Config = require("../helpers/config");


const DEFAULTS = {
    host: "127.0.0.1",
    dialect: "mysql",
    timezone: "+00:00",
    define: {
        underscored: true,
        createdAt: "created_at",
        updatedAt: "updated_at",
        deletedAt: "deleted_at"
    }
};


const migrationsTableName = (service) => {
    return `__${service}_migrations`;
};


const builder = (service, broker, customConfig, ...modelRefs) => {
    const models = {};
    const sequelizeInstance = new Sequelize({
        ...DEFAULTS,
        ...Config.get("db"),
        ...customConfig,
        migrationStorageTableName: migrationsTableName(service)
    });

    modelRefs.forEach(model => {
        let initializedModel = model.init(sequelizeInstance, Sequelize);
            initializedModel.broker = broker;

        models[camelCase(model.name)] = initializedModel;
    });

    Object.values(models).forEach(model => {
        if (typeof(model.associate) === "function") {
            model.associate(models);
        }

        if (typeof(model.relate) === "function") {
            model.relate(models);
        }

        if (typeof(model.enforceSegmentation) === "function") {
            model.enforceSegmentation();
        }

        model.applyHooks();

        if (typeof(model.bindHooks) === "function") {
            model.bindHooks(models);
        }
    });

    models.sequelize = sequelizeInstance;
    models.Sequelize = Sequelize;

    return models;
};


module.exports = {
    DEFAULTS,
    migrationsTableName,
    build: builder
};
