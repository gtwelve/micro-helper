const { ResourceMissingException } = require("../helpers/exceptions");


module.exports = {


    static: {


        paged(page, perPage, options) {
            let params = {
                ...options
            };

            if (perPage !== -1) {
                params.offset = (page - 1) * perPage;
                params.limit = perPage;
            }

            return this.findAndCountAll(params).then(result => {
                let data = {
                    page: 1,
                    per_page: -1,
                    count: result.count,
                    data: result.rows.map(i => i.serialize())
                };

                if (perPage !== -1) {
                    data.page = page;
                    data.per_page = params.limit;
                }

                return data;
            });
        },


        findOrFail(id, options) {
            return this.findOrFailWhere({ id: id }, options);
        },


        findOrFailWhere(where, options) {
            return this.findOneWhere(where, options).then(model => {
                if (!model) {
                    throw new ResourceMissingException();
                }

                return model;
            });
        },


        findOneWhere(where, options = {}) {
            return this.findOne({ where: where, ...options });
        },


        findOrInitialize(where, options = {}) {
            let opts = { where: where, ...options };

            return this.findOne(opts).then(model => {
                if (!model) {
                    model = this.build(where);
                }

                return model;
            });
        },


        findAndUpdate(where, data, options = {}) {
            return this.findOrInitialize(where, options).then(model => {
                model.fill(data);

                return model.save({
                    silent: (data.created_at || data.updated_at ? true : false)
                });
            });
        }




    }


};
