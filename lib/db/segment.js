

const SEGMENT_FIELD_KEY = "segmentField";
const SEGMENT_VALUE_KEY = "segmentValue";
const SEGMENT_BYPASS_KEY = "skipSegment";



module.exports = {



    instance: {



        hasSegmentation() {
            return typeof(this.constructor.options[SEGMENT_FIELD_KEY]) === "string";
        },


        getSegmentParams() {
            const field = this.constructor.options[SEGMENT_FIELD_KEY];
            const value = this.constructor.options[SEGMENT_VALUE_KEY] || this[this.constructor.options[SEGMENT_FIELD_KEY]];

            return {
                segmentField: field,
                segmentValue: value,
                [field]: value,
                [SEGMENT_BYPASS_KEY]: this.constructor.options[SEGMENT_BYPASS_KEY],
            };
        }



    },


    static: {


        enforceSegmentation() {
            if (this.options[SEGMENT_FIELD_KEY]) {
                this.beforeCreate(function(model, options) {
                    this._enforceModelHasSegment(model, options);
                });

                this.beforeBulkCreate(function(models, options) {
                    models.forEach(i => {
                        this._enforceModelHasSegment(i, options);
                    });
                });

                this.beforeFind(function(query) {
                    this._enforceQueryHasSegment(query);
                });

                this.beforeCount(function(query) {
                    this._enforceQueryHasSegment(query);
                });

                this.beforeBulkDestroy(function(query) {
                    this._enforceQueryHasSegment(query);
                });

                if (!this.options.immutableFields) {
                    this.options.immutableFields = [];
                }

                if (!this.options.immutableFields.includes(this.options[SEGMENT_FIELD_KEY])) {
                    this.options.immutableFields.push(this.options[SEGMENT_FIELD_KEY]);
                }

                // Skip all associations having to provide a segment value
                for (let key in this.associations) {
                    const superFunction = this.prototype["get" + key];

                    this.prototype["get" + key] = function(options) {
                        return superFunction.apply(
                            this,
                            [Object.assign({ skipSegment: true }, options)]
                        );
                    };
                }
            }
        },


        segment(params) {
            if (!params) {
                throw new Error("Segment params invalid");
            }

            const self = class extends this {};

            Object.defineProperty(self, "name", { value: this.name });
            Object.defineProperty(self, "options", { value: Object.assign(
                {},
                self.options
                // {
                //     [SEGMENT_VALUE_KEY]: params[self.options[SEGMENT_FIELD_KEY]],
                //     [SEGMENT_BYPASS_KEY]: params[SEGMENT_BYPASS_KEY] === true
                // }
            ) });

            if (this.options[SEGMENT_FIELD_KEY]) {
                self.applySegment(params);
            } else {
                throw new Error("no segment key!");
            }

            return self;
        },


        applySegment(params) {
            let klass = this;

            // console.log("apply segemnt params", params);

            this.options[SEGMENT_VALUE_KEY] = params[klass.options[SEGMENT_FIELD_KEY]],
            this.options[SEGMENT_BYPASS_KEY] = params[SEGMENT_BYPASS_KEY] === true

            klass.beforeCreate(function(model, options) {
                this._setSegmentValue(model, options);
            });

            klass.beforeBulkCreate(function(models, options) {
                models.forEach(i => {
                    this._setSegmentValue(i, options);
                });
            });

            klass.beforeFind(function(query) {
                return this._applySegmentWhere(query);
            });

            klass.beforeCount(function(query) {
                return this._applySegmentWhere(query);
            });

            klass.beforeBulkDestroy(function(query) {
                return this._applySegmentWhere(query);
            });
        },



        hasSegmentation() {
            return typeof(this.options[SEGMENT_FIELD_KEY]) === "string";
        },


        segmentationField() {
            return this.options[SEGMENT_FIELD_KEY] || null;
        },


        segmentationValue() {
            return this.options[SEGMENT_VALUE_KEY] || null;
        },






        _enforceModelHasSegment(model, options) {
            const bypass = this.options[SEGMENT_BYPASS_KEY] === true || options[SEGMENT_BYPASS_KEY] === true;

            if (!bypass && this.options[SEGMENT_FIELD_KEY] && !this.options[SEGMENT_VALUE_KEY] && !model.get(this.options[SEGMENT_FIELD_KEY])) {
                throw new Error(`Segment required for model ${this.name}, field: ${this.options[SEGMENT_FIELD_KEY]}`);
            }
        },



        _enforceQueryHasSegment(query) {
            const bypass = this.options[SEGMENT_BYPASS_KEY] === true || query[SEGMENT_BYPASS_KEY] === true;

            if (!bypass && this.options[SEGMENT_FIELD_KEY] && !this.options[SEGMENT_VALUE_KEY] && (!query.where || !query.where[this.options[SEGMENT_FIELD_KEY]])) {
                throw new Error(`Segment required for model ${this.name}, field: ${this.options[SEGMENT_FIELD_KEY]}`);
            }
        },



        _setSegmentValue(model, options) {
            const value = this.options[SEGMENT_VALUE_KEY];

            if (value) {
                model.set(this.options[SEGMENT_FIELD_KEY], value);
            }
        },


        _applySegmentWhere(query) {
            const bypass = this.options[SEGMENT_BYPASS_KEY] === true || query[SEGMENT_BYPASS_KEY] === true;

            if (!bypass) {
                if (!query.where) {
                    query.where = {};
                }

                if (!query.where[this.options[SEGMENT_FIELD_KEY]]) {
                    query.where[this.options[SEGMENT_FIELD_KEY]] = this.options[SEGMENT_VALUE_KEY];
                }
            }

            return query;
        }


    }



};
