const Sequelize = require("sequelize");
const { snakeCase } = require("snake-case");
const Moment = require("../helpers/moment");
const ValidationError = require("./validationError");


const associationMixin = require("./association");
const relatedMixin = require("./related");

const fillMixin = require("./fill");
const findersMixin = require("./finders");
const segmentMixin = require("./segment");
const searchMixin = require("./search");

const hooksMixin = require("./hooks");



class Model extends Sequelize.Model {


    serialize() {
        let data = {
            ...this.dataValues
        };

        for (let key in data) {
            if (this[key] instanceof Date) {
                data[key] = Moment(this[key]).serializeString();
            } else if (this[key] instanceof Moment) {
                data[key] = this[key].serializeString();
            }
        }

        Object.keys(this.constructor.associations).forEach(i => {
            if (data[i]) {
                if (Array.isArray(data[i])) {
                    data[snakeCase(i)] = data[i].map(o => o.serialize());

                    delete data[i];
                } else if (typeof data[i].serialize == "function") {
                    data[snakeCase(i)] = data[i].serialize();

                    delete data[i];
                }
            }
        });

        return data;
    }


    save(options) {
        return super.save(options).catch(err => {
            if (err instanceof Sequelize.ValidationError) {
                return Promise.reject(new ValidationError(err));
            }

            return Promise.reject(err);
        });
    }


    saveOrRestore() {
        if (this.isSoftDeleted()) {
            return this.restore();
        } else {
            return this.save();
        }
    }


    with(withNames) {
        return Promise.all([
            this.withAssociations(withNames),
            this.withRelated(withNames)
        ]).then(() => {
            return this;
        });
    }


    get Sequelize() {
        return Sequelize;
    }


    get Op() {
        return Sequelize.Op;
    }


    get wasNewRecord() {
        return this.previous(this.constructor.primaryKeyField) == null;
    }


    get persisted() {
        return !this.isNewRecord;
    }


    static init(fields, options) {
        for (let fieldName in fields) {
            if (fields[fieldName] == Sequelize.DATE) {
                fields[fieldName] = this._castDateField(fieldName, { type: Sequelize.DATE });
            } else if (fields[fieldName].type == Sequelize.DATE) {
                fields[fieldName] = this._castDateField(fieldName, fields[fieldName]);
            }
        }

        return super.init(fields, options);
        //
        // model.applyHooks();
        //
        // return model;
    }


    static _castDateField(fieldName, field) {
        field.get = function() {
            let value = this.getDataValue(fieldName);

            return value ?
                (value instanceof Moment) ? value.clone().utc() : Moment(value).utc() :
                null ;
        };

        field.set = function(value) {
            if (value) {
                value = ((value instanceof Moment) ? value : Moment(value)).utc().toDate();
            }

            this.setDataValue(fieldName, value);
        };

        return field;
    }


    static validationError(field, message) {
        let error = new Error();
            error.errors = [
                { path: field, message: message }
            ];

        return new ValidationError(error);
    }


}


// Define sequelize constants
Model.Sequelize = Sequelize;
Model.Op = Sequelize.Op;


// Apply Mixins
Object.assign(Model.prototype, associationMixin.instance);
Object.assign(Model, associationMixin.static);

Object.assign(Model.prototype, relatedMixin.instance);
Object.assign(Model, relatedMixin.static);

Object.assign(Model.prototype, segmentMixin.instance);
Object.assign(Model, segmentMixin.static);

Object.assign(Model, findersMixin.static);

Object.assign(Model, searchMixin.static);

Object.assign(Model, hooksMixin.static);

Object.assign(Model.prototype, fillMixin.instance);
Object.assign(Model, fillMixin.static);



module.exports = Model;
