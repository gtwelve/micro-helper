

module.exports = {


    instance: {


        fill(data) {
            let fillable = this.constructor.getFillableFields(this.isNewRecord == false);

            for (let key in data) {
                if (fillable.includes(key)) {
                    this.set(key, data[key]);
                }
            }

            return this;
        },


        forceFill(data) {
            for (let key in data) {
                this.set(key, data[key]);
            }

            return this;
        }


    },



    static: {


        getFillableFields(excludeImmutable) {
            let ignoredFields = ["id", "created_at", "updated_at"];
            let fillableFields = this.options && this.options.fillableFields ?
                this.options.fillableFields :
                Object.keys(this.rawAttributes);

            if (excludeImmutable === true) {
                let immutableFields = this.options && this.options.immutableFields ?
                    this.options.immutableFields :
                    [];

                ignoredFields = ignoredFields.concat(immutableFields);
            }

            return fillableFields.filter(i => ignoredFields.includes(i) == false);
        }


    }


};
