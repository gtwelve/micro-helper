const { ApplicationException } = require("../helpers/exceptions");


class ValidationError extends ApplicationException {


    constructor(error) {
        super(
            (error.errors && error.errors.length == 1 ? "A validation error occured." : "Validation errors occured."),
            400,
            "VALIDATION_EXCEPTION",
            null
        );

        this.data = this._getErrorData(error.errors);
    }


    _getErrorData(errors) {
        if (errors) {
            let simpleErrors = {};

            errors.forEach(i => {
                let stringValue = !i.value || i.value.toString() == "[object Object]" ? "value" : i.value.toString();
                let message = i.message ? i.message : `${stringValue} is not valid`;

                switch(i.validatorKey) {
                    case "len":
                        message = `must be between ${i.validatorArgs[0]} and ${i.validatorArgs[1]} characters`;
                        break;
                    case "is_null":
                        message = "is required";
                        break;
                    case "isIn":
                        message = `must be one of ${i.validatorArgs.join(", ")}`;
                        break;
                    case "notIn":
                        message = `must not be one of ${i.validatorArgs.join(", ")}`;
                        break;
                }

                simpleErrors[i.path] = message;
            });

            return simpleErrors;
        }

        return null;
    }


}


module.exports = ValidationError;
