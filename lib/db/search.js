const { camelCase } = require("lodash");


module.exports = {

    static: {


        getSearchableFields() {
            if (this._searchableFieldTypes === undefined) {
                this._searchableFieldTypes = Object.values(this.expandSearchTypes(this.options.searchableFields || []));
            }

            return this._searchableFieldTypes;
        },


        getSearchableAttributes() {
            if (this._searchableAttributeTypes === undefined) {
                this._searchableAttributeTypes = this.expandSearchTypes(this.options.searchableAttributes || []);
            }

            return this._searchableAttributeTypes;
        },


        getSearchableAssociations() {
            if (this._searchableAssociationTypes === undefined) {
                this._searchableAssociationTypes = this.expandAssociationTypes(this.options.searchableAssociations || []);
            }

            return this._searchableAssociationTypes;
        },


        applySearch(options) {
            if (options.search) {
                const searchableFields = this.getSearchableFields();
                const searchableAttributes = this.getSearchableAttributes();
                const searchableAssociations = this.getSearchableAssociations();
                const searchQuery = {};

                for (let key in options.search) {
                    let cleanKey = key.toLowerCase().trim();

                    if (cleanKey === "q" && typeof(options.search["q"]) === "string") {
                        let compoundOr = searchableFields.reduce((acc, field) => {
                            let value = this.getSearchValue(field, options.search["q"]);

                            if (value) {
                                acc.push({
                                    [field.key]: {
                                        [field.op]: value
                                    }
                                });
                            }

                            return acc;
                        }, []);

                        if (compoundOr.length > 0) {
                            searchQuery[this.Op.or] = compoundOr;
                        }
                    } else if (searchableAttributes[cleanKey]) {
                        let field = searchableAttributes[cleanKey];

                        this.getSearchConditions(searchQuery, field, options.search);

                    } else if (searchableAssociations[cleanKey]) {
                        if (!options.include) {
                            options.include = [];
                        }

                        let assoc = searchableAssociations[cleanKey];

                        options.include.push(this.getInclude(
                            assoc.associationName,
                            true,
                            [],
                            this.getSearchConditions({}, assoc, options.search),
                            { with: options.with }
                        ));
                    }
                }

                options.where = Object.assign(searchQuery, options.where);
            }

            return options;
        },


        applySort(options) {
            if (options.search && typeof(options.search.sort) == "string") {
                let parts = options.search.sort.split(" ");
                let direction = (parts[1] + "").toUpperCase() === "DESC" ? "DESC" : "ASC";

                if (parts[0] && this.rawAttributes[parts[0]]) {
                    options.order = [ [parts[0], direction] ];
                }
            } else {
                options.order = [ ["created_at", "ASC"] ];
            }

            return options;
        },


        getSearchConditions(searchQuery, field, searchParams) {
            let keyIndex = field.as || field.key;
            let value = Array.isArray(searchParams[keyIndex]) ?
                searchParams[keyIndex].map(i => this.getSearchValue(field, i)).filter(i => i !== null) :
                this.getSearchValue(field, searchParams[keyIndex]) ;

            if (Array.isArray(value)) {
                if (value.length > 0) {
                    searchQuery[this.Op.or] = value.reduce((acc, value) => {
                        acc.push({
                            [field.key]: {
                                [field.op]: value
                            }
                        });

                        return acc;
                    }, []);
                }
            } else if (value !== null) {
                searchQuery[field.key] = { [field.op]: value };
            }

            return searchQuery;
        },


        expandSearchTypes(fields) {
            return fields.reduce((acc, field) => {
                if (typeof(field) === "string") {
                    acc[field] = {
                        key: field,
                        type: this.tableAttributes[field].type,
                        op: this.getSearchOp(this.tableAttributes[field].type)
                    };
                } else {
                    acc[field.key] = { ...field };

                    if (acc[field.key].type === undefined) {
                        acc[field.key].type = this.tableAttributes[field.key].type;
                    }

                    if (acc[field.key].op === undefined) {
                        acc[field.key].op = this.getSearchOp(acc[field.key].type);
                    }
                }

                return acc;
            }, {});
        },


        expandAssociationTypes(associations) {
            return associations.reduce((acc, field) => {
                let key = field.as || field.key;
                let baseAssociation = this.associations[field.association];
                let associationMethod = camelCase(baseAssociation.associationType);
                let associationName = camelCase("search_" + key);
                let association = this[associationMethod](
                    baseAssociation.target,
                    {
                        ...baseAssociation.options,
                        as: associationName
                    }
                );

                acc[key] = {
                    ...field,
                    associationName,
                    association
                };

                if (acc[key].type === undefined) {
                    acc[key].type = association.target.tableAttributes[field.key];
                }

                if (acc[key].op === undefined) {
                    acc[key].op = this.getSearchOp(acc[key].type);
                }

                return acc;
            }, {});
        },


        getSearchOp(type) {
            let op = this.Op.eq;

            if (["STRING", "TEXT"].includes(type.key)) {
                op = this.Op.like;
            }

            return op;
        },


        getSearchValue(field, value) {
            let cleanValue = (value + "").toLowerCase().trim();

            if (cleanValue === "") {
                return null;
            }

            if (field.type.key === "BOOLEAN") {
                return cleanValue === "1" || cleanValue === "true" ? 1 : 0;

            } else if (["STRING", "TEXT"].includes(field.type.key)) {
                return (field.op === this.Op.like) ?
                    `%${cleanValue}%` :
                    cleanValue ;

            } else if (field.type.key === "FLOAT") {
                let floatValue = parseFloat(cleanValue);

                return isNaN(floatValue) ?
                    null :
                    floatValue ;
            } else if (["NUMERIC", "NUMBER", "TINYINT", "SMALLINT", "MEDIUMINT", "INTEGER", "BIGINT"].includes(field.type.key)) {
                let intValue = parseInt(cleanValue);

                return isNaN(intValue) ?
                    null :
                    intValue ;
            // } else if (field.type.key === "TIME") {
            } else if (field.type.key === "DATE") {
                let dateValue = Date.parse(cleanValue);

                return isNaN(dateValue) ?
                    null :
                    dateValue ;
            }

            return cleanValue;
        }


    }


};
