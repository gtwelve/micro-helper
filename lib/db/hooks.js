
module.exports = {


    static: {


        applyHooks() {
            const klass = this;
            
            this.beforeCount(function(options) {
                klass.applySearchHook(options);
            });

            this.beforeFind(function(options) {
                klass.applySearchHook(options);

                if (Array.isArray(options.with) || typeof(options.with) === "string") {
                    if (typeof(options.with) === "string") {
                        options.with = [options.with];
                    }
                    
                    klass.applyWithAssociations(options);
                }

                return options;
            });
            
            this.afterFind(function(results, options) {
                if (options.with) {
                    return klass.loadRelated(results, options.with, options);
                }

                return null;
            });
        },


        applySearchHook(options) {
            if (options.search) {
                this.applySearch(options);

                if (options.search.sort) {
                    this.applySort(options);
                }
            }
        }


    }


};
