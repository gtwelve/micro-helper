const { PersistanceException } = require("../helpers/exceptions");
const { upperFirst } = require("lodash");


module.exports = {


    instance: {


        reflectAssociation(name) {
            const model = this.constructor.associations[name] ?
                this.constructor.associations[name].target :
                null;

            if (model && model.hasSegmentation() && this.hasSegmentation()) {
                return model.segment(this.getSegmentParams());
            }

            return model;
        },


        withAssociations(withNames) {
            let names = this.constructor.withAssociationsNames(withNames);
            
            return Promise.all(names.map(i => this.withAssociation(i, { with: withNames }))).then(() => {
                return this;
            });
        },


        withAssociation(name, options = {}) {
            name = upperFirst(name);

            if (this.constructor.associations[name]) {
                return this[`get${name}`]({
                    ...options,
                    skipSegment: true
                }).then(result => {
                    return this.setAssociation(name, result);
                });
            }

            return Promise.resolve(this);
        },


        setAssociation(name, association) {
            let includeOptions = this.constructor._validateIncludedElements({
                include: [
                    this.constructor.getInclude(name)
                ]
            });

            this._options = Object.assign(this._options, includeOptions);

            this.set(name, association, { raw: true });
            this[name] = association;
            
            return this;
        }


    },



    static: {


        withAssociationsNames(withNames) {
            if (withNames && this.associations) {
                if (typeof(withNames) === "string") {
                    withNames = [withNames];
                } else if (Array.isArray(withNames)) {
                    withNames = withNames.filter(i => typeof(i) === "string");
                }

                return withNames
                    .map(name => upperFirst(name))
                    .filter(name => this.associations[name]);
            }

            return [];
        },


        applyWithAssociations(options) {
            let names = this.withAssociationsNames(options.with);
            if (names) {
                let includes = {};

                if (options.include) {
                    options.include.forEach(include => {
                        includes[include.as] = include;
                    });
                }

                names.forEach(name => {
                    let include = this.getInclude(name, false, null, null, { with: options.with });

                    if (!includes[include.as]) {
                        includes[include.as] = include;
                    }
                });

                options.include = Object.values(includes);
            }

            return options;
        },


        getInclude(modelName, required = false, attributes = null, where = null, options = null) {
            if (!this.associations[modelName]) {
                throw new PersistanceException(`Association '${modelName}' not found for ${this.name}`);
            }

            let model = this.associations[modelName].target;
            let include = {
                model: model,
                as: modelName,
                required: required
            };

            if (options && options.with) {
                include.include = model
                    .withAssociationsNames(options.with)
                    .map(i => {
                        return model.getInclude(i, false, null, null, { with: options.with });
                    });
            }

            if (attributes) { include.attributes = attributes; }
            if (where) { include.where = where; }
            if (options) { include = Object.assign(include, options); }

            return include;
        },


        validateAssociation(field, association, polymorphic) {
             return function(value) {
                 let associationRef = polymorphic === true ? this[association] : association;
                 let associationModel = this.reflectAssociation(associationRef);
                 let queryParams = {
                     where: {
                         id: value
                     }
                 };

                 return associationModel.count(queryParams).then(result => {
                     if (result === 0) {
                         throw this.constructor.validationError(field, "has an invalid association");
                     }

                     return true;
                 });
             };
        }


    }


};
