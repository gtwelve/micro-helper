const { uniq, upperFirst } = require("lodash");


module.exports = {



    instance: {


        setRelated(name, related) {
            return this.setDataValue(name, related);
        },


        withRelated(withNames) {
            return this.constructor.loadRelated(this, withNames).then(() => {
                return this;
            });
        }


    },


    static: {


        relatedToOne(name, serviceAction, options) {
            return this.setRelatedTo("one", name, serviceAction, options);
        },


        relatedToMany(name, serviceAction, options) {
            return this.setRelatedTo("many", name, serviceAction, options);
        },


        setRelatedTo(type, name, serviceAction, options) {
            if (this.relations === undefined) {
                this.relations = {};
            }

            let klass = this;
            let relation = this.relations[name] = {
                ...options,
                type,
                name,
                serviceAction
            };

            this.prototype["get" + upperFirst(name)] = function() {
                return klass.getRelation(this, relation).then(this.get(name));
            };
        },


        loadRelated(model, withNames, options) {
            let names = this.withRelatedNames(withNames);
            let promises = [];
            
            if (names.length > 0) {
                promises = names.map(i => {
                    return this.getRelation(model, {
                        with: withNames,
                        ...this.relations[i]
                    });
                });
            }

            if (options && options.includeNames && options.includeNames.length > 0) {
                options.includeNames.forEach(includeName => {
                    let includeOptions = options.includeMap[includeName];
                    let relatedNames = includeOptions.model.withRelatedNames(withNames);
                    let arrayReducer = (accumulator, value) => {
                        return accumulator.concat(
                            Array.isArray(value[includeName]) ?
                                value[includeName] :
                                [value[includeName]]
                        );
                    };
                    let relatedModel = Array.isArray(model) ? model.reduce(arrayReducer, []) : model[includeName];

                    relatedNames.forEach(relatedName => {
                        promises.push(
                            includeOptions.model.getRelation(relatedModel, {
                                with: withNames,
                                ...includeOptions.model.relations[relatedName]
                            })
                        );
                    });
                });
            }

            return Promise.all(promises);
        },


        getRelation(model, options) {
            let ids = Array.isArray(model) ?
                uniq(model.map(i => i[options.relatedKey])) :
                model[options.relatedKey];

            let params = { per_page: -1, [options.foreignKey]: ids };
            let callOptions = { meta: { skipSegment: true } };

            if (options.with) { params.with = options.with; }

            return this.broker.call(
                options.serviceAction,
                params,
                callOptions
            ).then(callResult => {
                if (Array.isArray(model) === false) {
                    model = [ model ];
                }

                for (let o = 0; o < model.length; o++) {
                    let resultData = options.type === "one" ?
                        callResult.data.find(i => i[options.foreignKey] === model[o][options.relatedKey]) :
                        callResult.data.filter(i => i[options.foreignKey] === model[o][options.relatedKey])

                    model[o].setRelated(options.name, resultData);
                }

                return options.type === "one" ?
                    callResult :
                    callResult.data ;
            });
        },


        withRelatedNames(withNames) {
            if (withNames && this.relations) {
                if (typeof(withNames) === "string") {
                    withNames = [withNames];
                } else if (Array.isArray(withNames)) {
                    withNames = withNames.filter(i => typeof(i) === "string");
                }

                return withNames.filter(name => this.relations[name]);
            }

            return [];
        }


    }


};
